"use strict";

class Validator{
    constructor(form){
        this.patterns = {
            name: /^[a-za-яё]+$/i,
            phone: /^\+7\(\d{3}\)\d{3}-\d{4}$/,
            mail: /^[\w._-]+@\w+\.[a-z]{2,4}$/i
        };
        this.errors = {
            name: 'Некорректно введенное имя',
            phone: 'Некорректно введен телефон',
            mail: 'Некорректно введен email'
        };
        this.errorClass = 'error-msg';
        this.form = form;
        this.valid = false;
        this._validateForm();
    }

    _validateForm(){
        let errors = [...document.getElementById(this.form).querySelectorAll(`.${this.errorClass}`)];
        for(let error of errors){
            error.remove();
        }
        let formFields = [...document.getElementById(this.form).getElementsByTagName('input')];
        for (let field of formFields){
            this._validate(field);
        }
        if(![...document.getElementById(this.form).querySelectorAll('.invalid')].length){
            this.valid = true;
        }
    }
    _validate(field){
        if(this.patterns[field.name]){
            if(!this.patterns[field.name].test(field.value)){
                field.classList.add('invalid');
                this._addErrorMsg(field);
                this._watchField(field);
            }
        }
    }
    _addErrorMsg(field){
        let errMsg = `<div class="${this.errorClass}">
                        ${this.errors[field.name]}
                    </div>`;
        field.parentNode.insertAdjacentHTML('beforeEnd', errMsg);
    }
    _watchField(field){
        field.addEventListener('input', () => {
            let error = field.parentNode.querySelector(`.${this.errorClass}`);
            if(this.patterns[field.name].test(field.value)){
                field.classList.remove('invalid');
                field.classList.add('valid');
                if(error){
                    error.remove();
                }
                else{
                    field.classList.remove('valid');
                    field.classList.add('invalid');
                    if(!error){
                        this._addErrorMsg(field);
                    }
                }
            }
        })
    }
}