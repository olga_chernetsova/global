import React, {Component} from 'react';
import {connect} from 'react-redux';
import Checkbox from './Checkbox';
import Card from './Card';

class GiftCard extends Component{

    hahdleChange(check){
        this.props.onCheck(check);
    }

    render(){
        return(
            <div className="card-block">
                <div className="title">Подарочная карта</div>
                <Checkbox isChecked={this.props.checkbox} hahdleChange = {this.hahdleChange.bind(this)}/>
            {
                this.props.checkbox ? <Card /> : null
            }
            </div>

        )
    }
}

export default connect(
    state => ({
        checkbox: state.checkbox
    }),
dispatch => ({
    onCheck: (check) => {
        dispatch({
            type: 'CHECKED',
            payload: check
        })
    }
})
)(GiftCard)