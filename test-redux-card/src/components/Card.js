import React, {Component} from 'react';
import {connect} from 'react-redux';
import MaskedInput from 'react-text-mask';
import {getCards} from '../actions/card';

class Card extends Component{

    handleApply(){
        event.preventDefault();
        var required = document.getElementsByClassName("required");
        var numbCard = document.getElementById("numbCard");
        var codeCard = document.getElementById("codeCard");
        var validate = function(field) {
            var valid;
            for (var i = 0; i < field.length; i++) {
                 (function (field, i) {
                    if (field[i].value == '') {
                        field[i].classList.add("error");
                        valid = false;
                    }
                    else {
                        field[i].classList.remove("error");
                        valid = true;
                    }
                })(field, i);
            }
            return valid;
        };
        const func = validate(required);

        if(func){
            this.props.onError(false);
            this.props.onGetCards('mockapi.json', numbCard.value.replace(/\s/g, ''), codeCard.value);
            numbCard.value = '';
            codeCard.value = '';
        }
        else{
            this.props.onError(true); 
        }
    }

    render(){
        console.log(this.props);

        return(
            <div>
                <div className="text-light title-card">
                    Пожалуйста, введите номер карты и её код
                </div>
        {
            (this.props.card.length > 0) ?

            this.props.card.map((data, index) =>{
                    var start = "";
                    var str="";
                    var pattern = "**** **** **** ";
                    for(var n=12; n<data.numb.length; n++){
                        str +=  data.numb[n];
                    }
                    start += pattern + str;

            return <div className="card-apply">
                        <div className="card-apply__title">Подарочная карта</div>
                        <div className="card-apply__data">
                            <div className="card-apply__numb">
                                {start}
                            </div>
                            <div className="card-apply__discount">
                                {data.discount}
                            </div>
                        </div>
                    </div>;
            })
            :  null
        }

            {
                !this.props.fetch ?
                <div className="card-apply__error">
                    <sup>*</sup>Карта не найдена
                </div>
                : null
            }

                <form className="card-wrap">
                    <div className="card-item">
                        <MaskedInput id="numbCard" className="card-field required" autocomplete="off" placeholder="Номер карты" mask={[/\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/]} />
                    </div>
                    <div className="card-item">
                        <input id="codeCard" className="card-field required" autocomplete="off" placeholder="Код карты" maxlength="4" type="text" />
                    </div>
                {
                    this.props.error ?
                        <div className="card-apply__error">
                            <sup>*</sup>Не корректно заполнены данные
                        </div>
                    : null
                }

                    <div>
                        <button className="card-btn" type="submit" onClick={this.handleApply.bind(this)}>Применить</button>
                    </div>
                </form>
            </div>
        )
    }

}

export default connect(
    state => ({
        card: state.card,
        error: state.error,
        fetch: state.fetch
    }),
    dispatch => ({
        onError: (iserror) => {
            dispatch({
                type: 'ERROR',
                payload: iserror
            })
        },
        onGetCards: (url, numbCard, codeCard) =>{
            dispatch(getCards(url, numbCard, codeCard))
        }
    })

)(Card)