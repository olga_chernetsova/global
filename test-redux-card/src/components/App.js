import React, {Component} from 'react';
import GiftCard from './GiftCard';

class App extends Component{

    render(){
        return(
            <div className="bg-body">
                <GiftCard />
            </div>
        )
    }
}

export default App;