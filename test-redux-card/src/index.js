import React from 'react';
import ReactDom from 'react-dom';
import {Provider} from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import './css/style.scss';
import App from './components/App';
import reducers from './reducers/index';


const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)));

ReactDom.render(
<Provider store = {store}>
    <App />
    </Provider>,
    document.getElementById('app')
)