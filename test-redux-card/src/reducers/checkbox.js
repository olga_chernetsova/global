export default function checkbox(state=true, action){
    switch (action.type){
        case 'CHECKED':
            return action.payload;
        default: return state;
    }
}