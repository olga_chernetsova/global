export default function fetch (state=true, action){
    switch (action.type) {
        case 'DELIVERY':
            return action.payload;
        default: return state;
    }
}