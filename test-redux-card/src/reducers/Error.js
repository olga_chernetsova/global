export default function error (state=false, action){
    switch (action.type) {
        case 'ERROR':
            return action.payload;
        default: return state;
    }
}