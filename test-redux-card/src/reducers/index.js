import {combineReducers} from 'redux';

import checkbox from './checkbox';
import card from './Card';
import error from './Error';
import fetch from './Fetch'


export default combineReducers({
    checkbox,
    card,
    error,
    fetch
})