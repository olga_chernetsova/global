const initialState =[];

export default function card(state=initialState, action){
    switch (action.type) {
        case 'ADD_CARD':
            var newState = state.concat(action.payload);
            return newState;
        default: return state;
    }
}