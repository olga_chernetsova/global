export const getCards = (url, numbCard, codeCard) => dispatch => {
fetch(url)
.then((response) => {
    if (!response.ok) {
        throw new Error("HTTP error, status = " + response.status);
    }
    return response;
})
.then((response) => response.json())
.then((data) => {
        var result = [];
        for(var i=0; i<data.length; i++){
            if(data[i].numb == numbCard && data[i].code == codeCard){
                result.push({"numb":data[i].numb, "code": data[i].code, "discount": data[i].discount});
            }
        }
        return result;
})
.then((result) =>{
        if(result.length > 0){
            dispatch({type: 'DELIVERY', payload: true});
            dispatch({type: 'ADD_CARD', payload: result});
        }
        else{
            dispatch({type: 'DELIVERY', payload: false});
        }
})
.catch((error) =>  {
    console.log('Error Feth: ' + error);
});

}
