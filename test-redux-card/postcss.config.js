module.exports = {
    plugins: [
        require('autoprefixer')({}),
        require("postcss-nested")({}),
        require("postcss-short")({}),
        require("postcss-fontpath")({}),
        require("postcss-assets")({
            loadPaths: ['img/'],
            basePath:'./src/img/',
            relative: true
        }),
        require("postcss-cssnext")({
            browsers: ['last 2 versions', '> 5%']
        })
    ]
};