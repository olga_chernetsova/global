import React from 'react';
import { useNavigate } from 'react-router-dom';
 
export const AboutPage: React.FC = () => {
  const history = useNavigate();
  return <>
    <h1>О нас</h1>
    <p>Этот контент пока в разработке</p>
    <button className='btn' onClick={() => history('/')}>Оратно к списку дел</button>
  </>
}