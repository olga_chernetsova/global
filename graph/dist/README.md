Граф построен с помощью библиотеки D3.js
D3.js - javascript библиотека для визуализации данных с Html, SVG, CSS.
В состав D3 входит десяток макетов, охватывающих наиболее распространенные визуализации.
Их можно условно разделить на обычные и иерархические. Обычный макет представляет данные в виде плоской иерархии,
а иерархический - в виде древовидной структуры.
К обычным макетам относятся следующие:
  - гистограммы;
  - секторные диаграммы;
  - стопка;
  - хорда;
  - действующие силы.
К иерархическим относят следующие:
  - дерево;
  - кластер;
  - древовидная карта;
  - разбиение;
  - упаковка.

Иерархия должна быть вложенной совокупностью объектов, в которой иерархичность определяется неявно посредством
 некоторого атрибута (например, с именем children)

 ```json
    {
      "name": "flare",
      "children": [
        {
          "name": "analytics",
          "children": [
            {
              "name": "cluster",
              "children": [
                {"name": "AgglomerativeCluster", "size": 3938},
                {"name": "CommunityStructure", "size": 3812},
                {"name": "HierarchicalCluster", "size": 6714},
                {"name": "MergeEdge", "size": 743}
              ]
            }
        }
    }
 ```
 В любой такой последовательности должен быть один и только один корневой узел, т.е. узел, не имеюший родителей
 и являющийся предком всех остальных узлов.
 Такая структура объекта подходит для древовидной структуры данных. Для представления сетевого графика объект должен
 иметь следующую структуру:
 ```json
    {
      "nodes": [
        {"id": "Myriel"},
        {"id": "Napoleon"},
        {"id": "Mlle.Baptistine"},
        {"id": "Mme.Magloire"},
        {"id": "CountessdeLo"},
        {"id": "Geborand"},
        {"id": "Champtercier"},
        {"id": "Cravatte"},
        {"id": "Count"},
        {"id": "OldMan"},
        {"id": "Labarre"},
        {"id": "Valjean"},
        {"id": "Marguerite"},
        {"id": "Mme.deR"}
      ]
      "links": [
        {"source": "Napoleon", "target": "Myriel", "value": 1},
        {"source": "Mlle.Baptistine", "target": "Myriel", "value": 8},
        {"source": "Mme.Magloire", "target": "Myriel", "value": 10},
        {"source": "Mme.Magloire", "target": "Mlle.Baptistine", "value": 6},
        {"source": "CountessdeLo", "target": "Myriel", "value": 1},
        {"source": "Geborand", "target": "Myriel", "value": 1},
        {"source": "Champtercier", "target": "Myriel", "value": 1},
        {"source": "Cravatte", "target": "Myriel", "value": 1},
        {"source": "Count", "target": "Myriel", "value": 2},
        {"source": "OldMan", "target": "Myriel", "value": 1},
        {"source": "Valjean", "target": "Labarre", "value": 1},
        {"source": "Valjean", "target": "Mme.Magloire", "value": 3},
        {"source": "Valjean", "target": "Mlle.Baptistine", "value": 3},
        {"source": "Valjean", "target": "Myriel", "value": 5},
        {"source": "Marguerite", "target": "Valjean", "value": 1},
        {"source": "Mme.deR", "target": "Valjean", "value": 1},
        {"source": "Isabeau", "target": "Valjean", "value": 1},
        {"source": "Gervais", "target": "Valjean", "value": 1},
        {"source": "Listolier", "target": "Tholomyes", "value": 4},
        {"source": "Fameuil", "target": "Tholomyes", "value": 4},
        {"source": "Fameuil", "target": "Listolier", "value": 4}
      ]
    }
 ```
 где nodes - список узлов;
 links - список связей (source - узел источника ссылки; target - целевой узел ссылки; остальные параметры
 носят дополнительный информативный характер).