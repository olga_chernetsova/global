import * as d3 from 'd3';
import {contextMenu} from './contextMenu.js';
import './style.css';

const width = 960;
const height = 500;
let i = 0;
let link;
let node;

const simulation = d3.forceSimulation()
    .force('charge', d3.forceManyBody().strength(-50))
    .force('center', d3.forceCenter(width/2, height/2))
    .force('X', d3.forceX(width/2).strength(0.15))
    .force('Y', d3.forceY(height/2).strength(0.15));

const svg = d3.select('body')
    .append('svg')
    .attr('viewBox', [0, 0, width, height])
    .call(d3.zoom().on('zoom', function(event) {
      svg.attr('transform', event.transform);
    }))
    .append('g');

d3.json('../bd/data.json')
    .then((data) => {
      const root = d3.hierarchy(data);

      simulation
          .force('link', d3.forceLink().id(d => d.id).distance(80).strength(0.5))
          .on('tick', ticks);

      function update() {
        const nodes = flatten(root);
        const links = root.links();

        link = svg
            .selectAll('.link')
            .data(links, d => d.target.id);

        link.exit().remove();

        const linkEnter = link
            .enter()
            .append('line')
            .attr('class', 'link');

        link = linkEnter.merge(link);

        node = svg
            .selectAll('.node')
            .data(nodes, d => d.id);

        node.exit().remove();

        const nodeEnter = node
            .enter()
            .append('g')
            .attr('class', 'node')
            .attr('id', (d) => d.id)
            .attr('r', 3.5)
            .on('click', clicked)
            .on('contextmenu', (event, d) => {
              event.preventDefault();
              contextMenu(d);
            })
            .call(d3.drag()
                .on('start', dragstart)
                .on('drag', dragged));

        nodeEnter.append('circle')
            .style('fill', d => d._children ? '#3182bd' : d.children ? '#3182bd' : '#000000')
            .style('stroke', d => d._children ? '#3182bd' : d.children ? '#3182bd' : '#ffffff')
            .style('cursor', d => d._children ? 'pointer' : d.children ? 'pointer' : 'default')
            .attr('r', 3.5);

        nodeEnter.append('text')
            .attr('class', 'title')
            .attr('x', 6)
            .attr('text-anchor', d => d._children ? 'end' : 'start')
            .text(d => d.data.name)
            .clone(true).lower()
            .attr('class', 'title__layout');

        node = nodeEnter.merge(node);
        simulation.nodes(nodes);
        simulation.force('link').links(links);

      }

      function ticks() {
        link
            .attr('x1', d => d.source.x)
            .attr('y1', d => d.source.y)
            .attr('x2', d => d.target.x)
            .attr('y2', d => d.target.y);

        node
            .attr('transform', d => `translate(${d.x}, ${d.y})`);
      }

      function flatten(root) {
        const nodes = [];
        function recurse(node) {
          if (node.children) node.children.forEach(recurse);
          if (!node.id) node.id = ++i;
          else ++i;
          nodes.push(node);
        }
        recurse(root);
        return nodes;
      }

      function dragstart(event, d) {
        if (!event.active) simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
      }

      function dragged(event, d) {
        d.fx = event.x;
        d.fy = event.y;
        simulation.alpha(1).restart();
      }

      function clicked(event, d) {
        if (d.children) {
          d._children = d.children;
          d.children = null;
        } else {
          d.children = d._children;
          d._children = null;
        }
        update();
      }

      update();
    })
    .catch((error) => {
      console.log(error);
    });
