// var assert  = require("chai").assert;
var expect  = require("chai").expect;
var sinon = require('sinon');


describe("Тест 1", function(){
    it("Тест 1.1", function(){

        var result = {
            sum(x,y){
                return x+y;
            }
        };

        // sinon.stub(result, 'sum').returns(6);
        sinon.stub(result, 'sum');
        result.sum.withArgs(2, 2).callsFake(function() {
            return 6;
        });
        expect(result.sum(2, 2)).to.equal(6);
        // console.log(result.sum(2, 2));
    });
});