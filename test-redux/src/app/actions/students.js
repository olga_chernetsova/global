var mockApiData = [
    {
        "id": "03Std", "firstname": "Петя", "lastname": "Васечкин", "group": "5А"
    },
    {
        "id": "04Std", "firstname": "Маша", "lastname": "Сыроежкина", "group": "5Б"
    },
    {
        "id": "05Std", "firstname": "Вася", "lastname": "Петров", "group": "5А"
    },
    {
        "id": "06Std", "firstname": "Даша", "lastname": "Васина", "group": "5Б"
    }
]


export const getStd = () => dispatch => {
        setTimeout(() => {
            console.log('data received successfully');
            dispatch({type: 'FETCH_STUDENT_SUCCESS', payload: mockApiData})
        }, 2000)
}