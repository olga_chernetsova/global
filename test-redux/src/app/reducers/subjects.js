const initialState =
[
        {
            "id": "01", "name": "Математика"
        },
        {
            "id": "02", "name": "Русский язык"
        }
]

export default function subjectslist(state=initialState, action){
    if(action.type == 'ADD_SUBJECT'){
        return[
            ...state,
            action.payload
        ]
    }
    else if(action.type == 'DELETE_SUBJECT'){
        return Object.assign([], state, action.payload);

    }

    return state;
}