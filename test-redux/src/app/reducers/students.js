const initialState =
[
        {
            "id": "01Std", "firstname": "Петя", "lastname": "Иванов", "group": "5А"
        },
        {
            "id": "02Std", "firstname": "Маша", "lastname": "Сидорова", "group": "5Б"
        }
]

export default function studentslist(state=initialState, action){
    if(action.type == 'ADD_STUDENT'){
        return[
            ...state,
            action.payload
        ]
    }
    else if(action.type == 'FETCH_STUDENT_SUCCESS'){
        var newState = state.concat(action.payload);
        return newState;
    }
    return state;
}