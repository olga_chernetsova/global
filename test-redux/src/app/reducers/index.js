import {combineReducers} from 'redux';

import students from './students';
import subjects from './subjects';
import filterGroup from './filterGroup';

export default combineReducers({
    students,
    subjects,
    filterGroup
})