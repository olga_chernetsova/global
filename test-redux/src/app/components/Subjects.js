import React, {Component} from 'react';
import {connect} from 'react-redux';

class Subjects extends Component{

    addSub(){
        var names = {
            id: this.idSub.value,
            name: this.nameSub.value
        };

        this.props.onAddSub(names);
        this.idSub.value = "";
        this.nameSub.value = "";

        console.log(this.props.subjects);
    }

    delSub(){

        this.props.onDelSub(this.props.subjects,this.idSubDel.value);
        this.idSubDel.value = "";
    }

    render(){
        console.log(this.props.subjects);

        return(
    <React.Fragment>
        <ul className="nav nav-tabs mt-3 mb-3">
            <li className="nav-item">
                <a className="nav-link active" data-toggle="tab" href="#add-subject">Добавить</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" data-toggle="tab" href="#del-subject">Удалить</a>
            </li>
        </ul>
        <div className="tab-content mb-3">
            <div className="tab-pane container active" id="add-subject">
                <div>
                    <div className="form-group row mb-1">
                        <label for="idSub" className="col-sm-6 col-form-label">Введите id</label>
                        <div className="col-sm-6">
                            <input id="idSub" className="form-control-plaintext" type="text" placeholder="id" ref={(input)=>{this.idSub=input}} />
                        </div>
                    </div>
                    <div className="form-group row mb-1">
                        <label for="nameSub" className="col-sm-6 col-form-label">Введите Название</label>
                        <div className="col-sm-6">
                            <input id="nameSub" className="form-control-plaintext" type="text" placeholder="Название" ref={(input)=>{this.nameSub=input}} />
                        </div>
                    </div>
                    <button className="btn btn-primary" onClick={this.addSub.bind(this)}>Добавить предмет</button>
                </div>
            </div>
            <div className="tab-pane container" id="del-subject">
                <div>
                    <div className="form-group row mb-1">
                        <label for="idSubDel" className="col-sm-6 col-form-label">Введите id</label>
                        <div className="col-sm-6">
                            <input id="idSubDel" className="form-control-plaintext" type="text" placeholder="id" ref={(input)=>{this.idSubDel=input}} />
                        </div>
                    </div>
                    <button className="btn btn-primary" onClick={this.delSub.bind(this)}>Удалить предмет</button>
                </div>
            </div>
        </div>

        <table className="table table-striped mb-2">
            <tbody>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Название</th>
                </tr>
                {
                this.props.subjects.map((data, index) =>
                    <tr key={data.id}>
                        <td>{data.id}</td><td>{data.name}</td>
                    </tr>)
                }
            </tbody>
        </table>
    </React.Fragment>
    )
    }
}

export default connect(
    state => ({
    subjects: state.subjects
}),
dispatch=>({
    onAddSub: (names)=>{
        dispatch({
            type: 'ADD_SUBJECT',
            payload: names
        })
    },
    onDelSub: (sub, val) =>{
        var sub = sub;
        var val = val;
        for (var i=0; i<sub.length; i++){
            if(sub[i].id == val){
                sub.splice(i, 1);
            }
        }

        dispatch({
            type: 'DELETE_SUBJECT',
            payload: sub
        })
    }
})
)(Subjects);