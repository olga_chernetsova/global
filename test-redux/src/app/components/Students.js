import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getStd} from '../actions/students';

class Students extends Component{

    addStd() {
        var names = {
            id: this.id.value,
            firstname: this.firstname.value,
            lastname: this.lastname.value,
            group: this.group.value
        }

        this.props.onAddStd(names);
        this.id.value = "";
        this.firstname.value = "";
        this.lastname.value = "";
        this.group.value = "";
        console.log(this.props.students);
    }

    goFilterStd(){
        this.props.onFilterGroup(this.filterGroup.value);
        this.filterGroup.value = "";
    }

    render(){

        console.log(this.props.students);

        return(
            <React.Fragment>
                <ul className="nav nav-tabs mt-3 mb-3">
                    <li className="nav-item">
                        <a className="nav-link active" data-toggle="tab" href="#add">Добавить</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" data-toggle="tab" href="#getApi">Запрос</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" data-toggle="tab" href="#filter">Фильтр</a>
                    </li>
                </ul>
                <div className="tab-content mb-3">
                    <div className="tab-pane container active" id="add">
                        <div>
                            <div className="form-group row mb-1">
                                <label for="id" className="col-sm-6 col-form-label">Введите id</label>
                                <div className="col-sm-6">
                                    <input id="id" className="form-control-plaintext" type="text" placeholder="id" ref={(input)=>{this.id=input}} />
                                </div>
                            </div>
                            <div className="form-group row mb-1">
                                <label for="firstname" className="col-sm-6 col-form-label">Введите Имя</label>
                                <div className="col-sm-6">
                                    <input id="firstname" className="form-control-plaintext" type="text" placeholder="Имя" ref={(input)=>{this.firstname=input}} />
                                </div>
                            </div>
                            <div className="form-group row mb-1">
                                <label for="lastname" className="col-sm-6 col-form-label">Введите Фамилию</label>
                                <div className="col-sm-6">
                                    <input id="lastname" className="form-control-plaintext" type="text" placeholder="Фамилия" ref={(input)=>{this.lastname=input}} />
                                </div>
                            </div>
                            <div className="form-group row mb-1">
                                <label for="group" className="col-sm-6 col-form-label">Введите Группу</label>
                                <div className="col-sm-6">
                                    <input id="group" className="form-control-plaintext" type="text" placeholder="Группа" ref={(input)=>{this.group=input}} />
                                </div>
                            </div>
                            <button className="btn-add-std btn btn-primary" onClick={this.addStd.bind(this)}>Добавить студента</button>
                        </div>
                    </div>
                    <div className="tab-pane container" id="filter">
                        <div>
                            <div className="form-group row">
                                <label for="filterGroup" className="col-sm-6 col-form-label">Введите Группу</label>
                                <div className="col-sm-6">
                                    <input id="filterGroup" className="form-control-plaintext filterGroup" type="text" placeholder="Группа" ref={(input)=>{this.filterGroup=input}} />
                                </div>
                            </div>
                            <button className="btn-filter-group btn btn-primary" onClick={this.goFilterStd.bind(this)}>Отфильтровать</button>
                        </div>
                    </div>
                    <div className="tab-pane container" id="getApi">
                        <div>
                            <button className="btn btn-primary" onClick={this.props.onGetStd}>Получить список студентов</button>
                        </div>
                    </div>
                </div>

                <table className="table table-striped mb-2">
                    <tbody>
                        <tr>
                            <th scope="col">Имя</th>
                            <th scope="col">Фамилия</th>
                            <th scope="col">Группа</th>
                        </tr>
                    {
                        this.props.students.map((data, index) =>
                        <tr key={data.id}>
                            <td>{data.firstname}</td><td>{data.lastname}</td><td>{data.group}</td>
                        </tr>)
                    }
                    </tbody>
                </table>
        </React.Fragment>
    )
    }
}
export default connect(
    state => ({
    students: state.students.filter(student => student.group.includes(state.filterGroup))
}),
dispatch => ({
    onAddStd: (names)=>{
        dispatch({
            type: 'ADD_STUDENT',
            payload: names
        })
    },
    onFilterGroup: (group)=>{
        dispatch({
            type: 'FILTER_GROUP',
            payload: group
        })
    },
    onGetStd: () =>{
        dispatch(getStd());
    }
})
)(Students);