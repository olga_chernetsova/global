import React, {Component} from 'react';
import Students from './Students';
import Subjects from './Subjects';

class App extends Component{

    render(){

        return(
            <div className="container">
                <div className="row">
                    <div className="col-xs-12 col-sm-6">
                        <Students />
                    </div>
                    <div className="col-xs-12 col-sm-6">
                        <Subjects />
                    </div>
                </div>

            </div>
        )
    }
}
export default App;