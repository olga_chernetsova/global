import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDom from 'react-dom';
import {Provider} from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import App from './app/components/App';
import reducers from './app/reducers';


const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)));

ReactDom.render(
    <Provider store = {store}>
        <App />
    </Provider>,
    document.getElementById('app')
)