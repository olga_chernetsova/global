import React from 'react';

export const Loader = () => {
    return <div class="spinner-border text-danger" role="status">
    <span class="visually-hidden">Loading...</span>
  </div>
}