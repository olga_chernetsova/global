Vue.component('products',{
    data(){
        return{
            catalogUrl: 'catalog.json',
            filtered: [],
            products: []
        }
    },
    mounted(){
        this.$parent.getJson(`${this.catalogUrl}`)
            .then(data => {
            for(let item of data){
                this.$data.products.push(item);
                this.$data.filtered.push(item);
            }
        });
    },
    methods:{
        filter(userSearch){
            let regexp = new RegExp(userSearch, 'i');
            this.filtered = this.products.filter(el => regexp.test(el.name));
        }
    },
    template: `<div class="products">
                <product v-for="item of filtered" :key="item.id" :product="item" @add-product="$parent.$refs.cart.addProduct"></product>
               </div>`
});

Vue.component('product',{
    props: ['product'],
    template: `<div class="product-item">
                <img :src="product.img" class="img-responsive" alt="" />
                <div class="desc">
                    <h3>{{product.name}}</h3>
                    <p>{{product.price}} Руб.</p>
                    <button class="by-btn" @click="$emit('add-product', product)">Купить</button>
                </div>
            </div>`
});