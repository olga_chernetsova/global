Vue.component('cart', {
    data(){
        return{
            cartUrl: 'basket.json',
            cartItems: [],
            showCart: false
        }
    },
    mounted(){
            this.$parent.getJson(`${this.cartUrl}`)
                .then(data => {
                for(let item of data){
                this.$data.cartItems.push(item);
            }
        })
    },
    methods: {
        addProduct(item){
            this.$parent.getJson('addToBasket.json')
                .then(data => {
                let find = this.cartItems.find(el => el.id === item.id);
                if(find){
                    find.quantity++;
                }
                else{
                    const prod = Object.assign({quantity: 1}, item);
                    this.cartItems.push(prod);
                }
            })
        },
        remove(item){
            this.$parent.getJson('addToBasket.json')
                .then(data => {
                if(item.quantity > 1){
                item.quantity--;
            }
            else{
                    this.cartItems.splice(this.cartItems.indexOf(item), 1);
                }
            })
        }
    },
    template: `
                <div>
                    <div>
                        <button class="btn-cart" type="button" @click="showCart = !showCart">Корзина</button>
                    </div>
                    <div class="cart-block" v-show="showCart">
                        <p v-if="!cartItems.length">Корзина пуста</p>
                        <cart-item v-for="item of cartItems" :key="item.id" :cart-item="item" @remove="remove"></cart-item>   
                    </div>
                </div>`
});

Vue.component('cart-item',{
    props: ['cartItem'],
    template: `<div class="cart-item">
                <div class="product-bio">
                    <img :src="cartItem.img" alt="" class="cart-img"/>
                    <div class="product-desc">
                        <div class="product-title">{{cartItem.name}}</div>
                        <div class="product-quantity">Количество: {{cartItem.quantity}}</div>
                        <div class="product-single-price">{{cartItem.price}} Руб./шт</div>
                    </div>
                </div>
                <div class="right-block">
                    <span class="product-price">{{cartItem.quantity * cartItem.price}} Руб.</span>
                    <button class="del-btn" @click="$emit('remove', cartItem)">&times;</button>
                </div>
            </div>`
});