Vue.component('filter-elem',{
    data(){
        return{
            userSearch: ''
        }
    },
    template:`
            <div class="cart">
                <form action="#" class="search-form" @submit.prevent="$parent.$refs.products.filter(userSearch)">
                    <input type="text" class="search-field" v-model="userSearch"/>
                    <button type="submit" class="btn-search">
                        <i class="fas fa-search"></i>
                    </button>
                </form>
            </div>`
});