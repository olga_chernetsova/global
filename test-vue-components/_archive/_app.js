const app = new Vue({
    el: '#app',
    data:{
        title: 'Vue app',
        product: [
            {name: 'Chair', price: 100},
            {name: 'Mouse', price: 50},
            {name: 'Notebook', price: 1500},
        ],
        tabs: ['one', 'two', 'three'],
        currentTab: 'one'
    },
    computed: {
        currentTabComp(){
            return `comp-${this.currentTab}`
        }
    },
    methods:{
        remove(item){
            this.product.splice(this.product.indexOf(item), 1)
        }
    }
});