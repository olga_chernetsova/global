Vue.component('product',{
    props:['productList'],
    template: `<div><div class="product" v-for="item of productList" :key="item.name">
                    {{item.name}} - {{item.price}}
                    <button class="remove" @click="$emit('remove', item)">&times;</button>
                </div></div>`
});

Vue.component('comp-one', {
    template: `<div>1. Lorem impsum ...</div>`
});

Vue.component('comp-two', {
    template: `<div>2. Lorem impsum ...</div>`
});

Vue.component('comp-three', {
    template: `<div>3. Lorem impsum ...</div>`
});
