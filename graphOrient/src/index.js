import * as d3 from 'd3';
import './style.css';
import {setLinks, setNodes} from './modules/createNodes';
import {createGraph} from './modules/createGraph';

d3.json('../bd/data.json')
    .then((data) => {
      setLinks(data.links);
      setNodes(data.nodes);
      createGraph();
    })
    .catch((error) => {
      console.log(error);
    });
