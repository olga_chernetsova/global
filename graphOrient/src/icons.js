export const icons = {
  folder: 'folder-open-regular',
  article: 'file-alt-regular',
  source: 'link-solid',
  user: 'user-regular'
};
