import * as d3 from 'd3';

export function createTooltip(data) {
  const info = data.info;
  const content = () => {
    let html = '';
    for (const key in info) {
      if (Object.prototype.hasOwnProperty.call(info, key)) {
        html += '<p>' + info[key]['title'] + ': ' + info[key]['value'] + '</p>';
      }
    }
    return html;
  };

  const tooltip = () => {
    d3.select('.tooltip').remove();

    const block = d3.select('body')
        .append('div')
        .attr('class', 'tooltip')
        .style('opacity', 0);

    block.transition().duration(300).style('opacity', 1);

    block.append('div')
        .attr('class', 'tooltip__content')
        .html(content());
  };
  return tooltip();
}
