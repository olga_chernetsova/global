import * as d3 from 'd3';
import {renderGraph} from './renderGraph';

const width = 960;
const height = 500;

export const simulation = d3.forceSimulation()
    .force('charge', d3.forceManyBody().strength(-50))
    .force('center', d3.forceCenter(width/2, height/2))
    .force('X', d3.forceX())
    .force('Y', d3.forceY());

export const svg = d3.select('body')
    .append('svg')
    .attr('class', 'canvasGraph')
    .attr('viewBox', [0, 0, width, height])
    .call(d3.zoom().on('zoom', function(event) {
      svg.attr('transform', event.transform);
    }))
    .append('g');

export function createGraph() {
  renderGraph(simulation, svg);
}
