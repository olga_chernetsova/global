import * as d3 from 'd3';
import {deleteNode} from './createNodes';
import {renderGraph} from './renderGraph';
import {simulation, svg} from './createGraph';

const items = [
  {
    title: 'Удалить узел',
    action: (data) => {
      deleteNode(data.id);
      renderGraph(simulation, svg);
    }
  },
  {
    title: 'Добавить узел',
    action: () => {
      console.log('нажата копка Добавить узел');
    }
  },
  {
    title: 'Редактировать узел',
    action: () => {
      console.log('нажата копка Редактировать узел');
    }
  }
];

const width = 150;
const height = 30;

export function contextMenu(data) {
  const x = data.x;
  const y = data.y;
  const menu = () => {
    d3.select('.context-menu').remove();

    d3.select('svg')
        .append('g').attr('class', 'context-menu')
        .selectAll('tmp')
        .data(items).enter()
        .append('g').attr('class', 'menu-entry')
        .on('mouseover', function() {
          d3.select(this)
              .select('rect')
              .transition()
              .duration(200)
              .style('fill', '#dce6f8');
        })
        .on('mouseout', function() {
          d3.select(this)
              .select('rect')
              .transition()
              .duration(200)
              .style('fill', 'rgb(244,244,244)');
        });

    d3.selectAll('.menu-entry')
        .append('rect')
        .attr('class', 'menu-entry__item')
        .attr('x', x)
        .attr('y', (d, i) => y + (i * height))
        .attr('rx', 2)
        .attr('width', width)
        .attr('height', height)
        .on('click', (event, d) => d.action(data));

    d3.selectAll('.menu-entry')
        .append('text')
        .attr('class', 'menu-entry__title')
        .text((d) => d.title)
        .attr('x', x)
        .attr('y', (d, i) => y + (i * height))
        .attr('dx', 20)
        .attr('dy', 20)
        .on('click', (event, d) => d.action(data));

    d3.select('body')
        .on('click', function() {
          d3.select('.context-menu').remove();
        });
  };

  return menu();
}
