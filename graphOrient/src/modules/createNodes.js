let links = null;
let nodes = null;

export function setLinks(objLinks) {
  links = objLinks.map(d => Object.create(d));
}

export function getLinks() {
  return links;
}

export function setNodes(objNodes) {
  nodes = objNodes.map(d => Object.create(d));
}

export function getNodes() {
  return nodes;
}

export function deleteNode(dataNode) {
  nodes = nodes.filter((item) => item.id !== dataNode);
  links = links.filter((item) => item.source.id !== dataNode && item.target.id !== dataNode);
}
