import * as d3 from 'd3';
import {icons} from '../icons';
import {getLinks, getNodes} from './createNodes';
import {createTooltip} from './createTooltip';
import {contextMenu} from './contextMenu';

let link;
let node;

export function renderGraph(simulation, svg) {
  const types = Array.from(new Set(getLinks().map(d => d.type)));
  const color = d3.scaleOrdinal(types, d3.schemeCategory10);

  simulation
      .force('link', d3.forceLink().id(d => d.id).distance(80).strength(0.5));

  svg.selectAll('*').remove();

  svg.append('svg:defs').selectAll('marker')
      .data(types)
      .join('svg:marker')
      .attr('id', d => `arrow-${d}`)
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 15)
      .attr('refY', -0.5)
      .attr('markerWidth', 6)
      .attr('markerHeight', 6)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('fill', color)
      .attr('d', 'M0,-5L10,0L0,5');

  link = svg.append('g')
      .attr('fill', 'none')
      .attr('stroke-width', 1.5)
      .selectAll('path')
      .data(getLinks())
      .join('path')
      .attr('stroke', d => color(d.type))
      .attr('marker-end', d => `url(${new URL(`#arrow-${d.type}`, location)})`);

  node = svg.append('g')
      .attr('stroke-width', 1.5)
      .selectAll('.node')
      .data(getNodes())
      .join('g')
      .attr('class', 'node');

  node.append('circle')
      .attr('r', 5)
      .attr('fill', '#ffffff');

  node.append('svg:image')
      .attr('xlink:href', drawIcon)
      .style('stroke', '#ffffff')
      .style('fill', '#000000')
      .attr('x', -5)
      .attr('y', -5)
      .style('height', 10)
      .style('width', 10)
      .style('cursor', 'pointer')
      .on('mouseover', (event, d) => {
        link.classed('disabled', l => d.id !== l.source.id);
        d3.selectAll('.title').classed('disabled', t => t.id !== d.id);
        markerDisabled();
        createTooltip(d);
      })
      .on('mouseout', (event, d) => {
        link.classed('disabled', false);
        d3.selectAll('.title').classed('disabled', false);
        markerUndisabled();
        d3.select('.tooltip').remove();
      })
      .on('contextmenu', (event, d) => {
        event.preventDefault();
        contextMenu(d);
      });

  node.append('text')
      .attr('class', 'title')
      .attr('x', 8)
      .attr('y', '0.31em')
      .text(d => d.id)
      .clone(true).lower()
      .attr('class', 'title__layout');

  setTimeout(function() {
    link.attr('d', linkArc);
    node.attr('transform', d => `translate(${d.x}, ${d.y})`);
  }, 100);

  simulation.nodes(getNodes());
  simulation.force('link').links(getLinks());

  function drawIcon(d) {
    let icon = 'icons/circle-solid.svg';
    Object.keys(icons).forEach((key) => {
      if (d.type == key) {
        icon = `icons/${icons[key]}.svg`;
      }
    });
    return icon;
  }

  function linkArc(d) {
    const r = Math.hypot(d.target.x - d.source.x, d.target.y - d.source.y);
    return `M${d.source.x}, ${d.source.y} A${r},${r} 0 0,1 ${d.target.x}, ${d.target.y}`;
  }

  function markerDisabled() {
    const marker = document.querySelectorAll('marker');
    const disabledPath = document.querySelectorAll('path.disabled');
    marker.forEach((marker) => {
      if (marker.classList.contains('disabled')) {
        marker.classList.remove('disabled');
      }
      const id = marker.getAttribute('id');
      const str = new RegExp(id);
      disabledPath.forEach((path) => {
        const markEnd = path.getAttribute('marker-end');
        if (str.test(markEnd)) {
          marker.classList.add('disabled');
        }
      });
    });
  }

  function markerUndisabled() {
    const marker = document.querySelectorAll('marker');
    marker.forEach((marker) => {
      if (marker.classList.contains('disabled')) {
        marker.classList.remove('disabled');
      }
    });
  }
}
