$(document).ready(function(){

    $(".item-catalog").on('click', function(){
        var item = $(this);
        var ctrlc = item.html();
        $(".modal-container").html(ctrlc);
        $('.block-modal').show();
    });

    $(".btn-close").on('click', function(){
        $(this).closest('.block-modal').hide();
    });

});