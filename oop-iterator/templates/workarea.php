<?php

class Catalog implements Iterator, ArrayAccess{

    private $catalog = array();

    public function __construct(){
         $this->catalog = array(
            'title' => 'Фотогалерея',
            'catalog' => array(
                array(
                    'title' => 'Осень',
                    'path' => 'autumn',
                    'img' => array('autumn1.jpg','autumn2.jpg','autumn3.jpg','autumn4.jpg','autumn5.jpg','autumn6.jpg'),
                ),
                array(
                    'title' => 'Весна',
                    'path' => 'spring',
                    'img' => array('spring1.jpg','spring2.jpg','spring3.jpg','spring4.jpg','spring5.jpg','spring6.jpg','spring7.jpg'),
                ),
                array(
                    'title' => 'Лето',
                    'path' => 'summer',
                    'img' => array('summer1.jpg','summer2.jpg','summer3.jpg','summer4.jpg','summer5.jpg','summer6.jpg'),
                ),
                array(
                    'title' => 'Зима',
                    'path' => 'winter',
                    'img' => array('winter1.jpg','winter2.jpg','winter3.jpg','winter4.jpg','winter5.jpg','winter6.jpg'),
                ),
            ),
        );
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->catalog[] = $value;
        } else {
            $this->catalog[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->catalog[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->catalog[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->catalog[$offset]) ? $this->catalog[$offset] : null;
    }

    public function rewind(){
        reset($this->catalog);
    }

    public function current(){
        return current($this->catalog);
    }

    public function key(){
       return key($this->catalog);
    }

    public function next(){
        return next($this->catalog);
    }

    public function valid(){
        $key = key($this->catalog);
        return ($key !== null && $key !== false);
    }
}

$catalog = new Catalog();

?>

<div class="fotos">
    <div class="container">
            <h1 class="upp text-center"><?=$catalog['title']?></h1>

            <div class="catalog">
<?

nextArr($catalog);

function nextArr($arr){
    foreach ($arr as $v){
        if(is_array($v)){
            foreach($v as $arr_v){
                ?>

                <h2><?=$arr_v['title']?></h2>

                <div class="catalog-wrap">
                    <div class="item-catalog">
                <?
                if(is_array($arr_v)){

                foreach($arr_v['img'] as $v_img){
                ?>

                        <img src="imgs/<?=$arr_v['path']?>/<?=$v_img?> " class="img-responsive" alt="<?=$arr_v['title']?>" title="<?=$arr_v['title']?>" />
                    <?
                }
                ?>
                     </div>
                </div>
            </div>
    </div>
</div>
                    <?
                }
            }
        }
    }
}


?>