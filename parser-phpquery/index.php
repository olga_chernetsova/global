<?php

//Порционный парсинг нескольких страниц.Рекурсия

require 'lib/phpQuery.php';

function print_arr($arr){
    echo '<pre>'.print_r($arr, true).'</pre>';
}

function parser($url, $start, $end){

    if($start < $end){
        $file = file_get_contents($url);
        $doc = phpQuery::newDocument($file);

        foreach($doc->find('.articles-container .post-excerpt') as $articles){
            $articles = pq($articles);

            $img = $articles->find('.img-cont img')->attr('src');
            $text = $articles->find('.pd-cont')->html();

            echo "<img src='$img'>";
            echo $text;
            echo '<hr />';


        }

        $next = $doc->find('.page-nav .current')->next()->attr('href');
        if(!empty($next)){
            $start++;
            parser($next,$start,$end);
        }
    }
}

$url = 'http://www.kolesa.ru/news';
$start = 0;
$end = 2;

parser($url, $start, $end);



?>