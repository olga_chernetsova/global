function add(a: number, b: number): number {
  return a + b;
}

function toUpperCase(s: string): string {
  return s.trim().toUpperCase()
}

//-----------
interface IPosition {
  x: number | undefined,
  y: number | undefined
}
interface IPositionDefault extends IPosition{
  default: string
}

function position(): IPosition;
function position(a: number): IPositionDefault;
function position(a: number, b: number): IPosition;

function position(a?: number, b?: number) {
  if (a && !b) {
    return {x: a, y: undefined, default: a.toString()}
  }
  return {x:a, y:b}
}

console.log('Empty', position());
console.log('One params', position(10));
console.log('two params', position(10, 15));