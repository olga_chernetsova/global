var str = 'Hello';
var isFetching = true;
var isLoading = false;
var int = 42;
var float = 4.2;
var num = 3e10;
var numberArray = [1, 1, 2];
var numberArray2 = [1, 1, 2];
var words = ['Hello'];
var contact = ['Olga', 1234567]; //Tuple
//--------- type Any --------
var variable = 42;
variable = 'Any string';
variable = [];
// ---------------------------
function sayMyName(name) {
    console.log(name);
}
sayMyName('Olga');
//----------type Never (возвращает всегла ошибку или бесконечный цикл)--------
function throwError(message) {
    throw new Error(message);
}
function infinity() {
    while (true) { }
}
var login = 'admin';
var id1 = 123;
var id2 = '123';
