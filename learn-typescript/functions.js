function add(a, b) {
    return a + b;
}
function toUpperCase(s) {
    return s.trim().toUpperCase();
}
function position(a, b) {
    if (a && !b) {
        return { x: a, y: undefined, "default": a.toString() };
    }
    return { x: a, y: b };
}
console.log('Empty', position());
console.log('One params', position(10));
console.log('two params', position(10, 15));
