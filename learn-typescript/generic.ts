const arrNumb: Array<number> = [1, 1, 2];
const arrStr: Array<string> = ["Hello", "Word"];

function reverse<T>(array: T[]): T[] {
  return array.reverse();
}

reverse(arrNumb);
reverse(arrStr);