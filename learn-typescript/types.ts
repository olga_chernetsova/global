const str: string = 'Hello';

const isFetching: boolean = true;
const isLoading: boolean = false;

const int: number = 42;
const float: number = 4.2;
const num: number = 3e10;

const numberArray: number[] = [1, 1, 2];
const numberArray2: Array<number> = [1, 1, 2];

const words: string[] = ['Hello'];

const contact: [string, number] = ['Olga', 1234567]; //Tuple

//--------- type Any --------
let variable: any = 42;
variable = 'Any string';
variable = [];
// ---------------------------

function sayMyName(name: string): void {
  console.log(name);
}

sayMyName('Olga');

//----------type Never (возвращает всегла ошибку или бесконечный цикл)--------
function throwError(message: string): never {
  throw new Error(message)
}

function infinity(): never {
  while(true) {}
}
//-----------------------------

//----------type Type------------
type Login = string;
const login: Login = 'admin';

type ID = string | number;
const id1: ID = 123;
const id2: ID = '123';

type SomeType = string | null | undefined;