var Membership;
(function (Membership) {
    Membership[Membership["Simple"] = 0] = "Simple";
    Membership[Membership["Standard"] = 1] = "Standard";
    Membership[Membership["Premium"] = 2] = "Premium";
})(Membership || (Membership = {}));
var membership = Membership.Standard;
var membershipReverse = Membership[2];
console.log(membership, membershipReverse);
var socialMedia;
(function (socialMedia) {
    socialMedia["VK"] = "VK";
    socialMedia["FACEBOOK"] = "FACEBOOK";
    socialMedia["INSTAGRAM"] = "INSTAGRAM";
})(socialMedia || (socialMedia = {}));
var social = socialMedia.INSTAGRAM;
console.log(social);
