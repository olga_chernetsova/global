import { ChessComponents } from "../../core/ChessComponents";

export class Pawn extends ChessComponents{
  static shape = '<i class="fas fa-chess-pawn"></i>';

  constructor(style) {
    super();
    this.style = style;
  }

  toHtml() {
    return `
      <div class="${this.style}">${Pawn.shape}</div>
    `;
  }
}