import { ChessComponents } from "../../core/ChessComponents";

export class King extends ChessComponents{
  static shape = '<i class="fas fa-chess-king"></i>';

  constructor(style) {
    super();
    this.style = style;
  }

  toHtml() {
    return `
      <div class="${this.style}">${King.shape}</div>
    `;
  }
}