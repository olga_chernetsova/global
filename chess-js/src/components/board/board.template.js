import { Horse } from '../horse/Horse';
import { King } from '../king/King';
import { Pawn } from '../pawn/Pawn';
import { Queen } from '../queen/Queen';
import { Tower } from '../tower/Tower';

const CODES = {
  A: 65,
  H: 72  
}

function createCell(row) {
  return function (_, col) {
    let id = String.fromCharCode(CODES.A + col) + (row + 1);
    let share;
    switch(id) {
      case 'A1':
      case 'H1': 
        share = new Tower('dark');
        break;
      case 'A8':
      case 'H8':
        share = new Tower('light rotate');
        break;
      case 'B1':
      case 'G1':
        share = new Horse('dark');
        break;
      case 'B8':
      case 'G8': 
        share = new Horse('light rotate');
        break;
      case 'C1':
      case 'F1': 
        share = new King('dark');
        break;
      case 'C8':
      case 'F8':
        share = new King('light rotate');
        break;
      case 'D1':
      case 'E1': 
        share = new Queen('dark');
        break;
      case 'D8':
      case 'E8':
        share = new Queen('light rotate');
        break;
      case 'A2':
      case 'H2':
      case 'B2':
      case 'G2':
      case 'C2':
      case 'F2':
      case 'D2':
      case 'E2':
        share = new Pawn('dark');
        break;
      case 'A7':
      case 'H7':
      case 'B7':
      case 'G7':
      case 'C7':
      case 'F7':
      case 'D7':
      case 'E7':
        share = new Pawn('light rotate');
        break;
      default: share = {toHtml() { return ''}}; 
    }
    return `
      <div class="cell" data-id="${id}">
        ${share.toHtml()}
      </div>
    `
  }
}

function createCol(col, param) {
  return `
    <div class="column ${param}">${col}</div>
  `
}

function createRow(index, content) {
  return `
    <div class="row">
        <div class="row-info">${index ? index : ''}</div>
        <div class="row-game">${content}</div>
        <div class="row-info rotate">${index ? index : ''}</div>
    </div>
  `
}

function toChar(_, index) {
  return String.fromCharCode(CODES.A + index);
}

export function createBoard(rowsCount = 8) {
  const colsCount = CODES.H - CODES.A + 1;
  const rows = [];
  const cols = new Array(colsCount)
    .fill('')
    .map(toChar)
    .map(el => {
        return createCol(el, '');
    })
    .join('');

  rows.unshift(createRow(null, cols));

  for (let row = 0; row < rowsCount; row++) {
    const cells = new Array(colsCount)
      .fill('')
      .map(createCell(row))
      .join('')
    rows.unshift(createRow(row + 1, cells));
  }

  const colsRevers = new Array(colsCount)
    .fill('')
    .map(toChar)
    .map(el => {
        return createCol(el, 'rotate');
    });

  rows.unshift(createRow(null, colsRevers.join('')));

  return rows.join('');
}