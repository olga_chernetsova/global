// import {$} from '../../core/dom';
import {ChessComponents} from '../../core/ChessComponents';
import { createBoard } from './board.template';

export class Board extends ChessComponents {
  static className = 'chess__board';

  toHtml() {
    return createBoard();
  }
}
