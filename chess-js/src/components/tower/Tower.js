import { ChessComponents } from "../../core/ChessComponents";
import { $ } from "../../core/dom";

export class Tower extends ChessComponents {
  static shape = '<i class="fas fa-chess-rook"></i>';

  constructor(style) {
    super();
    this.style = style;
  }

  toHtml() {
    return `
      <div class="${this.style}">${Tower.shape}</div>
    `;
  }
}