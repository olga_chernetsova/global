import { ChessComponents } from "../../core/ChessComponents";

export class Horse extends ChessComponents{
  static shape = '<i class="fas fa-chess-knight"></i>';

  constructor(style) {
    super();
    this.style = style;
  }

  toHtml() {
    return `
      <div class="${this.style}">${Horse.shape}</div>
    `;
  }
}