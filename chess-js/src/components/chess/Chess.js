import {$} from '../../core/dom';

export class Chess {
  constructor(selector, options) {
    this.$el = document.querySelector(selector);
    this.components = options.components || [];
  }

  getRoot() {
    const $root = $.create('div', 'chess');

    this.components.forEach(Component => {
      const $el = $.create('div', Component.className);
      const component = new Component($el);
      $el.innerHTML = component.toHtml();
      $root.append($el);
    });
    return $root;
  }

  render() {
    this.$el.append(this.getRoot());
    console.log(this.$el);
  }
}
