import { ChessComponents } from "../../core/ChessComponents";

export class Queen extends ChessComponents{
  static shape = '<i class="fas fa-chess-queen"></i>';

  constructor(style) {
    super();
    this.style = style;
  }

  toHtml() {
    return `
      <div class="${this.style}">${Queen.shape}</div>
    `;
  }
}