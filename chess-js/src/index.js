import './scss/index.scss';
import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'

import {Chess} from './components/chess/Chess';
import {Board} from './components/board/Board';
import {Info} from './components/info/Info';

const chess = new Chess('#app', {
  components: [Board, Info]
});

chess.render();

