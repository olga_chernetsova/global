import '@babel/polyfill'
import app from './components/app'
import './css/style.css'
import './lib/fontawesome-free-5.12.0-web/css/all.css'

const application = new Vue(app);