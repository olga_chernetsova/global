import cart from './Cart'
import products from './Product'
import error from './Error'
import filterElem from './Filter'


const app = {
    el: '#app',
    data: {},
    components: {
        cart,
        products,
        error,
        filterElem
    },
    methods: {
        getJson(url){
            return fetch(url)
                    .then(result => result.json())
        .catch(error => {
                this.$refs.error.text=error;
        })
        },
        postJson(url, data){
            return fetch(url, {
                    method: 'POST',
                    headers:{
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify(data)
                })
                    .then(result => result.json())
        .catch(error => {
                this.$refs.error.text=error;
        })
        },
        putJson(url, data){
            return fetch(url, {
                    method: 'PUT',
                    headers:{
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify(data)
                })
                    .then(result => result.json())
        .catch(error => {
                this.$refs.error.text=error;
        })
        },
        deleteJson(url){
            return fetch(url, {
                    method: 'DELETE',
                    headers:{
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify(data)
                })
                    .then(result => result.json())
        .catch(error => {
                this.$refs.error.text=error;
        })
        }
    }
};

export default app;


// "use strict";
//
// const app = new Vue({
//     el: '#app',
//     data: {},
//     methods: {
//         getJson(url){
//             return fetch(url)
//                     .then(result => result.json())
//             .catch(error => {
//                 this.$refs.error.text=error;
//             })
//         },
//         postJson(url, data){
//             return fetch(url, {
//                 method: 'POST',
//                     headers:{
//                     "Content-Type": "application/json"
//                     },
//                     body: JSON.stringify(data)
//                 })
//                     .then(result => result.json())
//             .catch(error => {
//                     this.$refs.error.text=error;
//             })
//         },
//         putJson(url, data){
//             return fetch(url, {
//                     method: 'PUT',
//                     headers:{
//                         "Content-Type": "application/json"
//                     },
//                     body: JSON.stringify(data)
//                 })
//                     .then(result => result.json())
//             .catch(error => {
//                     this.$refs.error.text=error;
//             })
//         },
//         deleteJson(url){
//             return fetch(url, {
//                     method: 'DELETE',
//                     headers:{
//                         "Content-Type": "application/json"
//                     },
//                     body: JSON.stringify(data)
//                 })
//                     .then(result => result.json())
//             .catch(error => {
//                     this.$refs.error.text=error;
//             })
//         }
//     }
// });
//
