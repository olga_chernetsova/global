const express = require('express');
const fs = require('fs');
const app = express();
const cart = require('./cartRouter');

app.use(express.json());
app.use('', express.static('dist/public'));
app.use('/api/catalog', cart);

app.get('./public/api/products', (req, res) => {
    fs.readFile('dist/server/db/products.json', 'utf-8', (err, data) => {
        if(err){
            res.sendStatus(404, JSON.stringify({result: 0, text: err}));
        }
        else{
            res.send(data);
        }
    });
});

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`listen on port ${port} ...`));