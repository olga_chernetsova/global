<?php

require 'lib/phpQuery.php';

function get_content($url){
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $res = curl_exec($ch);
    curl_close($ch);
    return $res;
}

function parser($url, $start, $end){

    if($start < $end){
        $file = get_content($url);
        $doc = phpQuery::newDocument($file);

        foreach($doc->find('.articles-container .post-excerpt') as $articles){
            $articles = pq($articles);

            $img = $articles->find('.img-cont img')->attr('src');
            $text = $articles->find('.pd-cont')->html();

            echo "<img src='$img'>";
            echo $text;
            echo '<hr />';


        }

        $next = $doc->find('.pages-nav .current')->next()->attr('href');
        if( !empty($next) ){
            $start++;
            parser($next,$start,$end);
        }
    }
}

$url = 'http://www.kolesa.ru/news';
$start = 0;
$end = 2;

parser($url, $start, $end);

?>