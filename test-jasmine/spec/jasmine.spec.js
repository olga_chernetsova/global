describe('Соответствие значений', () => {
    it('Проверка а на значение 10', () => {
        let a = 10;
        expect(a).toBe(10);

        a = 9;
        expect(a).not.toBe(10);
    });
});

describe('Дополнительные функции', () => {
    it('Сравнение объектов', () => {
        let user1 = {name: 'Ann'};
        let user2 = {name: 'Ann'};

        expect(user1).toEqual(user2);
    });

    it('arrays', () => {
        let arr = ['black', 'white'];

    expect(arr).toContain('white');
    });
});