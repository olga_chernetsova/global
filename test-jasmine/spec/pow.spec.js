const pow = require('../pow');

describe('Возведение в степень', () => {
    it('Возведение 2 в степень 3', () =>{
        expect(pow(2,3)).toBe(8);
    });
    it('Возведение 2 в степень 5', () =>{
        expect(pow(2,5)).toBe(32);
    });
    it('Возведение 3 в степень 3', () =>{
        expect(pow(3,3)).toBe(27);
    });
});

describe('Нестандартные ситуации', () => {
    it('Если число null, то функция возвращает null', () =>{
        expect(pow(null,3)).toBeNull();
    });
    it('Если степень null, то функция возвращает null', () =>{
        expect(pow(2,null)).toBeNull();
    });
});