const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    entry: './src/index.js',
    mode: 'production',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
            rules: [
                {
                    test: /\.scss$/,
                    use: ['style-loader', MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader']
                },
                {
                             test: /\.(png|svg|jpg|gif)$/,
             use: [
               'file-loader'
                 ]
       },
                {
                             test: /\.(woff|woff2|eot|ttf|otf)$/,
             use: [
              'file-loader'
                ]
       },
                {
                    test: /\.m?js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: "babel-loader"
                    }
                }
     ]
    },

    plugins: [
        new MiniCssExtractPlugin({
            filename: 'style.css'
        })
    ]
};