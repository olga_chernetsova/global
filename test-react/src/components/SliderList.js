import React, {Component} from "react";
import Slider from "./Slider";

class SliderList extends Component{

    constructor(props){
        super(props);
        this.state = { slideIndex: 0};
        this.nextSlide = this.nextSlide.bind(this);
    }

    nextSlide(){
        const sliders = this.props.sliders;
        let newIndex = this.state.slideIndex;
        let dots = document.getElementsByClassName('dots-item');

        newIndex += 1;

            if(newIndex > sliders.length-1){
                newIndex = 0;
            }
            else if(newIndex < 0){
                newIndex = sliders.length-1;
            }

        this.setState({slideIndex: newIndex});
    }

    clickDots(index){
        this.setState({slideIndex: index});
    }

    mousNext(e){

    }

    render(){
        const sliderElements = this.props.sliders.map((slider, index) =>
            <Slider slider = {slider} defaultOpen={index===this.state.slideIndex} onmouseup={this.mousNext.bind(this)}/>)
        const sliderDots = this.props.sliders.map((slider, index) =>
            <div className={"dots-item " + (index===this.state.slideIndex ? 'active':'')} onClick={() => this.clickDots(index)}></div>)
        return(
            <div className="slider-block">
                {sliderElements}
                <div className="dots-block">
                    {sliderDots}
                </div>
            {
                this.state.slideIndex !=2 ?
    <div className="arrow-next" onClick={this.nextSlide.bind(this)}></div>
            :null
            }

            </div>
        )
    }
}

export default SliderList



