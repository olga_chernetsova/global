import React from "react";
import SliderList from "./SliderList";
import sliders from "../info";

function App(){
    return(
            <SliderList sliders={sliders}/>
        )
}

export default App