import React, {Component} from "react";

class ToggleList extends Component{

    constructor(props){
        super(props);
        this.state = { isActive: props.defaultActive};
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.defaultActive != this.props.defaultActive) this.setState({
            isActive: nextProps.defaultActive
        })
    }


    render(){
        const {toggle} = this.props;
            return(
            this.state.isActive && <div id={toggle.id} className='toggle'><img src={'../src/img/'+toggle.img} alt="" title="" /></div>
            )
    }

}

export default ToggleList