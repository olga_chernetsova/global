import React, {Component} from "react";
import ToggleList from "./ToggleList";

class Slider extends Component{

    constructor(props){
        super(props)

        this.state={
            isOpen: props.defaultOpen,
            toggleIndex: 2,
        }
    }

    componentWillMount(){
        console.log(this.props);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.defaultOpen != this.props.defaultOpen) this.setState({
            isOpen: nextProps.defaultOpen
        })
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleInput(event) {
        let val = this.state.value;
        let tgl = this.state.toggleIndex;
        if(val >= 0 && val <=1988){
            tgl=0;
        }
        else if(val >= 1988 && val <= 2009){
            tgl=1;
        }
        else if(val >= 2009 && val <= 2016){
            tgl=2;
        }
        this.setState({toggleIndex: tgl});
        console.log(this.state.value);
    }

    render(){

    const {slider} = this.props;
        return (
            this.state.isOpen && <div key={slider.id} id={slider.id} className="slide-item">
                <div className="slide-item__container">
                    <div className="slide-item__title title">{slider.title}</div>
                    {
                        slider.rounds?
                        slider.rounds.map((rounds)=>(
                            <div>
                        <div id={rounds.id} className="round round-big"><div className="round round-small"></div></div>
                        <span className={rounds.id+'-label label-round text-blue'}>{rounds.title}</span>
                            </div>
                        )) : null
                    }
        {
                    slider.toggle?
            slider.toggle.map((toggle, index) =>(
                    <ToggleList toggle={toggle} defaultActive={index===this.state.toggleIndex}/>))
        :null
        }
        {
            slider.years ?
        <div class="toggle-block">
            <input type="range" id="range" value={this.state.value}  min="1988" max="2016" step="3" onChange={this.handleChange.bind(this)} onInput={this.handleInput.bind(this)}/>
            <div class="year-block">
                <span id="year-01" class="year">1988</span>
                <span id="year-02" class="year">2009</span>
                <span id="year-03" class="year">2016</span>
            </div>
        </div>
            :null
        }
                </div>
            </div>
            )
    }


}

export default Slider