export default [
    {
        'id':'slide-01',
        'title':'Всегда ли цели терапии СД2 на поверхности?',
         'rounds':[
            {'id':'round-01', 'title':'Цель по HbAc1'},
            {'id':'round-02', 'title':'Гипогликемия'},
            {'id':'round-03', 'title':'Осложнения СД'},
            {'id':'round-04', 'title':'СС риски'}
        ]

    },
    {
        'id':'slide-02',
        'title':'Основа терcапии — патогенез СД2'
    },
    {
        'id':'slide-03',
        'title':'Звенья патогенеза СД2',
        'toggle':[
            {'id':'toggle-slide01', 'img':'slide3_1.png'},
            {'id':'toggle-slide02', 'img':'slide3_2.png'},
            {'id':'toggle-slide03', 'img':'slide3_3.png'}
        ],
        'years':[
            {'id':'year-01', 'title':'1988'},
            {'id':'year-02', 'title':'2009'},
            {'id':'year-03', 'title':'2016'}
        ]
    }
]