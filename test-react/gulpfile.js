var gulp = require('gulp'), //Подключаем Gulp;
    iconfont = require('gulp-iconfont'), //Подключаем генератор шрифтов
    runTimestamp = Math.round(Date.now()/1000),
    image = require('gulp-image'); // Оптимизация картинок
    gutil = require("gulp-util");

    gulp.task('iconfont', function(){
    return gulp.src(['src/fonts/*.ttf'])
        .pipe(iconfont({
            fontName: 'font', // required
            prependUnicode: true, // recommended option
            formats: ['svg', 'ttf', 'eot', 'woff', 'woff2'], // default, 'woff2' and 'svg' are available
            timestamp: runTimestamp // recommended to get consistent builds when watching files
        }))
        .on('glyphs', function(glyphs, options) {
            // CSS templating, e.g.
            console.log(glyphs, options);
        })
        .pipe(gulp.dest('src/fonts/'));
    });

    gulp.task('image', function () {
        gulp.src('src/img/*')
            .pipe(image())
            .pipe(gulp.dest('src/img/'));
    });